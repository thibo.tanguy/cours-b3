using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cours_b3.App.Models.Entities;

[Table("tags")]
public class Tag
{
    [Key]
    [Required]
    [Column("id")]
    public required int Id { get; set; }
    public required string Name { get; set; }
    public ICollection<SoundFileTag> SoundFileTags { get; set; } = new List<SoundFileTag>();
}