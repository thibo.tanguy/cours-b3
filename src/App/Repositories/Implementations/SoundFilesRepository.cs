using cours_b3.App.Contexts;
using cours_b3.App.Models.Dtos;
using cours_b3.App.Models.Entities;
using cours_b3.App.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace cours_b3.App.Repositories.Implementations;

public class SoundFilesRepository(ApplicationDbContext dbContext) : ISoundFilesRepository
{
    public async Task<SoundFile[]> FindAll()
    {
        return await dbContext.SoundFiles
            .Include(soundFile => soundFile.SoundFileTags)
            .ThenInclude(soundFile => soundFile.Tag)
            .ToArrayAsync();
    }
    
    public async Task<SoundFile?> FindById(int id)
    {
        return await dbContext.SoundFiles
            .Include(soundFile => soundFile.SoundFileTags)
            .ThenInclude(soundFile => soundFile.Tag)
            .FirstOrDefaultAsync(soundFile => soundFile.Id == id);
    }

    public async Task<SoundFile[]> FindByTagsExclusive(TagDto[] tags)
    {
        var tagIds = tags.Select(tag => tag.Id).ToList();
        
        var soundFiles = await dbContext.SoundFiles
            .Where(sf => sf.SoundFileTags
                .GroupBy(sft => sft.SoundFileId)
                .Where(sftGroup => sftGroup.All(sft => tagIds.Contains(sft.TagId)) && sftGroup.Count() == tagIds.Count)
                .Select(sftGroup => sftGroup.Key)
                .Contains(sf.Id))
            .Include(sf => sf.SoundFileTags)
            .ThenInclude(sft => sft.Tag)
            .ToArrayAsync();

        return soundFiles;
    }
    
    public async Task<SoundFile[]> FindByTagsNonExclusive(TagDto[] tags)
    {
        var tagIds = tags.Select(tag => tag.Id).ToList();
        var tagCount = tagIds.Count;

        var soundFiles = await dbContext.SoundFiles
            .Where(sf => sf.SoundFileTags
                .Where(sft => tagIds.Contains(sft.TagId))
                .GroupBy(sft => sft.SoundFileId)
                .Where(sftGroup => sftGroup.Count() == tagCount)
                .Select(sftGroup => sftGroup.Key)
                .Contains(sf.Id))
            .Include(sf => sf.SoundFileTags)
            .ThenInclude(sft => sft.Tag)
            .ToArrayAsync();
        
        // var soundFiles = await dbContext.SoundFileTags
        //     .Where(x => tags.Contains(x.TagId))
        //     .GroupBy(x => x.SoundFileId, (x, y) => new { id = x, ctp = y.Count() })
        //     .Where(x => x.ctp == tags.Count())
        //     .Join(dbContext.SoundFiles, x=>x.id, x=>x.Id, (x,y)=>y)
        //     .ToArrayAsync();

        return soundFiles;
    }
}