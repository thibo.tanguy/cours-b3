namespace cours_b3.App.Models.Dtos;

public class SoundFileDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }
    public TagDto[] Tags { get; set; }
}