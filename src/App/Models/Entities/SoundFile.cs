using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cours_b3.App.Models.Entities;

[Table("soundFiles")]
public class SoundFile
{
    [Key]
    [Required]
    [Column("id")]
    public int Id { get; set; }
    public required string Name { get; set; }
    public ICollection<SoundFileTag> SoundFileTags = new List<SoundFileTag>();
}