namespace cours_b3.App.Models.Dtos;

public class TagDto
{
    public required int Id { get; set; }
    public required string Name { get; set; }
}