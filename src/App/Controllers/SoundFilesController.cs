using cours_b3.App.Mappers;
using cours_b3.App.Models.Dtos;
using cours_b3.App.Services;
using Microsoft.AspNetCore.Mvc;

namespace cours_b3.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SoundFilesController(SoundFilesService soundFilesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<IEnumerable<SoundFileDto>>> GetSoundFiles()
    {
        var soundFiles = await soundFilesService.FindAll();
        var soundFilesDtos = soundFiles.Select(SoundFileMapper.MapToDto).ToList();
        
        return Ok(soundFilesDtos);
    }
    
    [HttpGet("{id:int}")]
    public async Task<ActionResult<SoundFileDto>> FindById(int id)
    {
        var soundFile = await soundFilesService.FindById(id);
        if (soundFile == null) return NotFound();
        var soundFileDto = SoundFileMapper.MapToDto(soundFile);

        return Ok(soundFileDto);
    }
    
    [HttpPost("nonexclusive")]
    public async Task<ActionResult<SoundFileDto[]>> GetNonExclusiveSoundFiles([FromBody] TagDto[] tags)
    {
        var soundFiles = await soundFilesService.FindByTagsNonExclusive(tags);
        var soundFilesDtos = soundFiles.Select(SoundFileMapper.MapToDto).ToList();

        return Ok(soundFilesDtos);
    }

    [HttpPost("exclusive")]
    public async Task<ActionResult<SoundFileDto[]>> GetExclusiveSoundFiles([FromBody] TagDto[] tags)
    {
        var soundFiles = await soundFilesService.FindByTagsExclusive(tags);
        var soundFilesDtos = soundFiles.Select(SoundFileMapper.MapToDto).ToList();

        return Ok(soundFilesDtos);
    }
}