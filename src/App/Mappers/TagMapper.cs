using cours_b3.App.Models.Dtos;
using cours_b3.App.Models.Entities;

namespace cours_b3.App.Mappers;

public abstract class TagMapper
{
    public static TagDto MapToDto(Tag tag)
    {
        return new TagDto
        {
            Id = tag.Id,
            Name = tag.Name,
        };
    }
}