﻿using cours_b3.App.Models.Entities;
using Microsoft.EntityFrameworkCore;
using SoundFile = cours_b3.App.Models.Entities.SoundFile;

namespace cours_b3.App.Contexts;

public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
{
    public DbSet<SoundFile> SoundFiles { get; set; }
    public DbSet<Tag> Tags { get; set; }
    public DbSet<SoundFileTag> SoundFileTags { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<SoundFileTag>()
            .HasKey(sft => new { SoundFileID = sft.SoundFileId, TagID = sft.TagId });

        modelBuilder.Entity<SoundFileTag>()
            .HasOne(sft => sft.SoundFile)
            .WithMany(sf => sf.SoundFileTags)
            .HasForeignKey(sft => sft.SoundFileId);

        modelBuilder.Entity<SoundFileTag>()
            .HasOne(sft => sft.Tag)
            .WithMany(t => t.SoundFileTags)
            .HasForeignKey(sft => sft.TagId);
    }
}