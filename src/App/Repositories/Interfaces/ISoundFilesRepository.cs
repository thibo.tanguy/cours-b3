using cours_b3.App.Models.Dtos;
using cours_b3.App.Models.Entities;

namespace cours_b3.App.Repositories.Interfaces;

public interface ISoundFilesRepository
{
    Task<SoundFile[]> FindAll();
    Task<SoundFile?> FindById(int id);
    Task<SoundFile[]> FindByTagsNonExclusive(TagDto[] tags);
    Task<SoundFile[]> FindByTagsExclusive(TagDto[] tags);
}