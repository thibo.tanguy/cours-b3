DROP DATABASE IF EXISTS cours_b3;
CREATE DATABASE cours_b3;
USE cours_b3;

-- Création de la table soundFiles
CREATE TABLE soundFiles
(
    Id   INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255) NOT NULL
);

-- Création de la table tags
CREATE TABLE tags
(
    Id   INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255) NOT NULL
);

-- Création de la table associative soundfiles_tag
CREATE TABLE soundfiles_tag
(
    SoundFileID INT,
    TagID       INT,
    PRIMARY KEY (SoundFileID, TagID),
    FOREIGN KEY (SoundFileID) REFERENCES soundFiles (Id) ON DELETE CASCADE,
    FOREIGN KEY (TagID) REFERENCES tags (Id) ON DELETE CASCADE
);

INSERT INTO soundFiles (Name)
VALUES ('SoundFile1'),
       ('SoundFile2'),
       ('SoundFile3'),
       ('SoundFile4'),
       ('SoundFile5'),
       ('SoundFile6'),
       ('SoundFile7'),
       ('SoundFile8'),
       ('SoundFile9'),
       ('SoundFile10'),
       ('SoundFile11'),
       ('SoundFile12'),
       ('SoundFile13'),
       ('SoundFile14'),
       ('SoundFile15'),
       ('SoundFile16'),
       ('SoundFile17'),
       ('SoundFile18'),
       ('SoundFile19'),
       ('SoundFile20'),
       ('SoundFile21'),
       ('SoundFile22'),
       ('SoundFile23'),
       ('SoundFile24'),
       ('SoundFile25'),
       ('SoundFile26'),
       ('SoundFile27'),
       ('SoundFile28'),
       ('SoundFile29'),
       ('SoundFile30');

INSERT INTO tags (Name)
VALUES ('Tag1'),
       ('Tag2'),
       ('Tag3'),
       ('Tag4'),
       ('Tag5'),
       ('Tag6'),
       ('Tag7'),
       ('Tag8'),
       ('Tag9'),
       ('Tag10'),
       ('Tag11'),
       ('Tag12'),
       ('Tag13'),
       ('Tag14'),
       ('Tag15'),
       ('Tag16'),
       ('Tag17'),
       ('Tag18'),
       ('Tag19'),
       ('Tag20'),
       ('Tag21'),
       ('Tag22'),
       ('Tag23'),
       ('Tag24'),
       ('Tag25'),
       ('Tag26'),
       ('Tag27'),
       ('Tag28'),
       ('Tag29'),
       ('Tag30');

INSERT INTO soundfiles_tag (SoundFileID, TagID) 
VALUES  (1, 1),
        (1, 2),
        (2, 1),
        (2, 3),
        (3, 2),
        (3, 3),
        (4, 4),
        (4, 5),
        (5, 6),
        (5, 7),
        (6, 8),
        (6, 9),
        (7, 10),
        (7, 11),
        (8, 12),
        (8, 13),
        (9, 14),
        (9, 15),
        (10, 16),
        (10, 17),
        (11, 18),
        (11, 19),
        (12, 20),
        (12, 21),
        (13, 22),
        (13, 23),
        (14, 24),
        (14, 25),
        (15, 26),
        (15, 27),
        (16, 28),
        (16, 29),
        (17, 30),
        (18, 1),
        (18, 2),
        (18, 3),
        (19, 3),
        (19, 4),
        (20, 5),
        (20, 6);
