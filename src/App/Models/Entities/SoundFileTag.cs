using System.ComponentModel.DataAnnotations.Schema;

namespace cours_b3.App.Models.Entities;

[Table("soundfiles_tag")]
public class SoundFileTag
{
    public required int SoundFileId { get; set; }
    public required SoundFile SoundFile { get; set; }
    public required int TagId { get; set; }
    public required Tag Tag { get; set; }
}