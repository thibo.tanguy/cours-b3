using cours_b3.App.Models.Dtos;
using cours_b3.App.Models.Entities;

namespace cours_b3.App.Mappers;

public abstract class SoundFileMapper
{
    public static SoundFileDto MapToDto(SoundFile soundFile)
    {
        return new SoundFileDto
        {
            Id = soundFile.Id,
            Name = soundFile.Name,
            Tags = soundFile.SoundFileTags.Select(soundFileTag => TagMapper.MapToDto(soundFileTag.Tag)).ToArray()
        };
    }
}