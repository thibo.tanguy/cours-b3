using cours_b3.App.Models.Dtos;
using cours_b3.App.Models.Entities;
using cours_b3.App.Repositories.Interfaces;

namespace cours_b3.App.Services;

public class SoundFilesService(ISoundFilesRepository soundFilesRepository)
{
    public async Task<SoundFile[]> FindAll()
    {
        return await soundFilesRepository.FindAll();
    }

    public async Task<SoundFile?> FindById(int id)
    {
        var soundFile = await soundFilesRepository.FindById(id);

        return soundFile;
    }
    
    public async Task<SoundFile[]> FindByTagsExclusive(TagDto[] tags)
    {
        return await soundFilesRepository.FindByTagsExclusive(tags);
    }
    
    public async Task<SoundFile[]> FindByTagsNonExclusive(TagDto[] tags)
    {
        return await soundFilesRepository.FindByTagsNonExclusive(tags);
    }
}